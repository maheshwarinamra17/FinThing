package services;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import database_layer.FinthingDB;
import database_layer.TagsTB;
import database_layer.TransactionTB;
import helpers.TransactionHelper;
import utils.HTTPCallHelper;
import utils.SMSConfig;
import utils.Utils;


public class TransactionsIntentService extends IntentService {

    FinthingDB dbConn;
    public TransactionsIntentService() {
        super("TransactionsIntentService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        dbConn = FinthingDB.createDBInstance(getApplicationContext());
        createTransactionsFromSms();
        insertTagsInLocal();
        assignTagsToTransactions();
        FinthingDB.destroyDBInstance();
    }

    private void createTransactionsFromSms(){
        System.out.println("createTransactionsFromSms: start");
        try {
            Long maxTS = dbConn.transactionTBDao().getLastSmsTS();
            if(maxTS == null)
                maxTS = Long.valueOf("0");
            ArrayList<TransactionTB> transactions = new TransactionHelper(getApplicationContext()).parseTransFromSMS(maxTS);
            dbConn.transactionTBDao().insertAll(transactions);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("createTransactionsFromSms: end");
    }

    private void insertTagsInLocal(){
        try {
            System.out.println("insertTagsInLocal: start");
            JSONObject tagData = HTTPCallHelper.makeGETCall(SMSConfig.API_TAGS_DATA);
            if(Utils.isNotEmpty(tagData)){
                Iterator<String> tagIdns = tagData.keys();
                ArrayList<String> oldTagsList = new ArrayList<>(dbConn.tagsTBDao().getAllIdns());
                ArrayList<TagsTB> newTagsList = new ArrayList<>();
                while( tagIdns.hasNext() ) {
                    String idn = tagIdns.next();
                    if(oldTagsList.contains(idn))
                        continue;
                    if(tagData.get(idn) instanceof JSONObject) {
                        JSONObject tagObject = tagData.getJSONObject(idn);
                        TagsTB tagsTB = new TagsTB();
                        tagsTB.setIdn(idn);
                        tagsTB.setName(tagObject.optString("name"));
                        tagsTB.setType(tagObject.optString("type"));
                        tagsTB.setCategory(tagObject.optString("category"));
                        newTagsList.add(tagsTB);
                    }
                }
                if(!newTagsList.isEmpty()){
                    dbConn.tagsTBDao().insertAll(newTagsList);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("insertTagsInLocal: end");
    }

    private void assignTagsToTransactions(){
        System.out.println("assignTagsToTransactions: start");
        List<TransactionTB> nonTaggedTrans = dbConn.transactionTBDao().getNonTaggedTrans();
        List<TagsTB> tags = dbConn.tagsTBDao().getTags();
        for(TransactionTB trans: nonTaggedTrans){
            for(TagsTB tag: tags){
                if(Utils.isNotEmpty(trans.getNarration()) && trans.getNarration().contains(tag.getIdn())){
                    trans.setTagID(tag.getId());
                    dbConn.transactionTBDao().update(trans);
                }
            }
        }
        System.out.println("assignTagsToTransactions: end");
    }
}
