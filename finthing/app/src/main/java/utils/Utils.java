package utils;

import android.content.Context;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import finthing.finthing.R;


/**
 * Created by namra on 24/06/18.
 */

public class Utils {

    public static boolean isNotEmpty(Object text){
        return text != null;
    }

    public static long stringTimeToEpoch(String format, String ts){
        long timestamp = 0;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            Date dt = sdf.parse(ts);
            timestamp = dt.getTime();
        } catch(ParseException e) {
            timestamp =  -1;
        }
        return timestamp;
    }

    public static String md5Hash(String target) {
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            md5.update(target.getBytes());
            StringBuffer sb = new StringBuffer();
            for (byte b : md5.digest())
                sb.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public static String sanatizeNumberString(String text){
        return text.replaceAll("Rs(\\.*)", "").replaceAll(",","").replaceAll("INR","");
    }

    public static String sanatizeNarrationString(String text){
        return text.replaceAll("[^A-Za-z0-9 ]", "");
    }

    public static int getColor(Context mCtx, int id){
        return mCtx.getResources().getColor(id);
    }

    public static Typeface fontNormal(Context mCtx){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            return mCtx.getResources().getFont(R.font.avenir_regular);
        }else{
            return Typeface.createFromAsset(mCtx.getAssets(), "font/avenir_regular.otf");
        }
    }

    public static Typeface fontBold(Context mCtx){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            return mCtx.getResources().getFont(R.font.avenir_bold);
        }else{
            return Typeface.createFromAsset(mCtx.getAssets(), "font/avenir_bold.otf");
        }
    }

    public static int getDayFromEpoch(long ts){
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(ts);
        return cal.get(Calendar.DAY_OF_MONTH);
    }

    public static String numberToIndianFormat(Double number){
        return NumberFormat.getCurrencyInstance(new Locale("en", "in")).format(number);
    }

    // PROGRAMATIC DRAWABLES

    public static Drawable drawableVerticleLine(Context mCtx, int colorId){
        GradientDrawable rectShape = new GradientDrawable();
        rectShape.setShape(GradientDrawable.RECTANGLE);
//        rectShape.setColor(Utils.getColor(mCtx, colorId));
        Integer[] colors = {R.color.red, R.color.green, R.color.yellow, R.color.purple, R.color.primary, R.color.grey_light};
        rectShape.setColor(Utils.getColor(mCtx, colors[new Random().nextInt(colors.length)]));
        return rectShape;
    }

    public static void showToast(String text){

    }

    public static void shadowedToolbar(Toolbar customToolbar){
        customToolbar.setElevation(4);
    }

}
