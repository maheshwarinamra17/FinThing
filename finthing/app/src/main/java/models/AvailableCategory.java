package models;

import finthing.finthing.R;

public enum AvailableCategory {

    TRAVEL("travel", R.color.red),
    LEISURE("leisure",R.color.purple),
    SAVINGS("savings",R.color.primary),
    SHOPPING("shopping",R.color.yellow);

    private String name;
    private int icon;

    AvailableCategory(String name, int icon) {
        this.name = name;
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public int getIcon() {
        return icon;
    }

    public static AvailableCategory getCategory(String value){
        for (AvailableCategory b : AvailableCategory.values()) {
            if (b.name.equalsIgnoreCase(value)) {
                return b;
            }
        }
        return null;
    }
}
