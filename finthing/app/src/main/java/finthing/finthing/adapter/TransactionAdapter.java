package finthing.finthing.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.apache.commons.lang3.StringUtils;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import database_layer.TagsTB;
import database_layer.TransactionTB;
import finthing.finthing.R;
import models.AvailableCategory;
import utils.Utils;

public class TransactionAdapter  extends RecyclerView.Adapter<TransactionAdapter.ViewHolder> {

    private List<TransactionTB> transactionList;
    private Context mCtx;
    private Map<Integer, TagsTB> tagListMapping;

    public TransactionAdapter(List<TransactionTB> transactionTBList, Context ctx, HashMap<Integer, TagsTB> tagTBMapping){
        transactionList = transactionTBList;
        mCtx = ctx;
        tagListMapping = tagTBMapping;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView narration, sub_narration, amount, trans_day;
        public ImageView cat_icon;
        public ViewHolder(View view) {
            super(view);
            cat_icon = (ImageView) view.findViewById(R.id.trans_cat_icon);
            narration = (TextView) view.findViewById(R.id.trans_narration);
            sub_narration = (TextView) view.findViewById(R.id.trans_sub_narration);
            amount = (TextView) view.findViewById(R.id.trans_amount);
            trans_day = (TextView) view.findViewById(R.id.trans_day);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_transaction, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TransactionTB transaction = transactionList.get(position);
        if(isShowDate(position)){
            holder.trans_day.setVisibility(View.VISIBLE);
            holder.trans_day.setText(formatDate(transaction.getTransTS()));
        }else{
            holder.trans_day.setVisibility(View.GONE);
        }
        holder.narration.setText(transaction.getNarration());
        String sub_narration = formatTime(transaction.getTransTS()) + (StringUtils.isNotEmpty(transaction.getLocation()) ? ", " + transaction.getLocation() : "");
        holder.sub_narration.setText(sub_narration);
        holder.amount.setText(transaction.getAmount().toString());
        setTagRelatedInfo(transaction.getTagID(), holder, transaction);
    }

    @Override
    public int getItemCount() {
        return Utils.isNotEmpty(transactionList) ? transactionList.size() : 0;
    }

    private void setTagRelatedInfo(Integer tagId, ViewHolder holder, TransactionTB transaction){
        if(Utils.isNotEmpty(tagId)){
            holder.narration.setText(tagListMapping.get(tagId).getName());
            AvailableCategory category = AvailableCategory.getCategory(tagListMapping.get(tagId).getCategory());
            if(Utils.isNotEmpty(category)) {
                holder.cat_icon.setImageDrawable(Utils.drawableVerticleLine(mCtx, category.getIcon()));
                String sub_narration = formatTime(transaction.getTransTS()) + ", " + tagListMapping.get(tagId).getType();
                holder.sub_narration.setText(sub_narration);
            }
        }
    }

    private String formatTime(long transTS){
        SimpleDateFormat tsFormat = new SimpleDateFormat("hh:mm a");
        Date longToDate = new Date(transTS);
        return tsFormat.format(longToDate);
    }

    private String formatDate(long transTS){
        SimpleDateFormat tsFormat = new SimpleDateFormat("dd-MMM-yyyy");
        Date longToDate = new Date(transTS);
        return tsFormat.format(longToDate);
    }

    private boolean isShowDate(int position){
        boolean result = false;
        if(position == 0)
            result = true;
        else{
            if(Utils.getDayFromEpoch(transactionList.get(position).getTransTS())
                    != Utils.getDayFromEpoch(transactionList.get(position-1).getTransTS())){
                result = true;
            }
        }
        return result;
    }

}
