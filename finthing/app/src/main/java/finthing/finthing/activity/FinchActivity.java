package finthing.finthing.activity;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import org.apache.commons.lang3.StringUtils;

import finthing.finthing.R;
import utils.Utils;

public class FinchActivity extends AppCompatActivity {

    EditText search;
    Button submit;
    LinearLayout chatInterface;
    Toolbar customToolbar;
    ScrollView chatInterfaceScroll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finch);
        prepareView();
        hookEvents();
    }

    private void prepareView(){
       search = (EditText) findViewById(R.id.etv_search);
       submit = (Button) findViewById(R.id.btn_searchq);
       chatInterface = (LinearLayout) findViewById(R.id.chat_interface);
       customToolbar = (Toolbar) findViewById(R.id.custom_toolbar);
       chatInterfaceScroll = (ScrollView) findViewById(R.id.chat_interface_scroll);
       Utils.shadowedToolbar(customToolbar);
    }

    private void hookEvents(){
        userSays();
    }

    private void userSays(){
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userQuery = search.getText().toString();
                if(StringUtils.isNotEmpty(userQuery)){
                    chatInterface.addView(textViewUserDialog(getApplicationContext(),userQuery));
                    search.setText("");
                }else{
                    Utils.showToast(getApplicationContext().getString(R.string.error_empty_query));
                }

            }
        });
    }

    public static TextView textViewUserDialog(Context mCtx, String text){
        GradientDrawable rectShape = new GradientDrawable();
        rectShape.setShape(GradientDrawable.RECTANGLE);
        rectShape.setColor(Utils.getColor(mCtx, R.color.grey_light));
        float[] radii = { 80, 80, 80, 80, 80, 80, 0, 0 };
        rectShape.setCornerRadii(radii);

        TextView uTv = new TextView(mCtx);
        uTv.setText(text);
        uTv.setPadding(32,16,32,16);
        uTv.setTextColor(Utils.getColor(mCtx, R.color.black));
        uTv.setBackground(rectShape);
        uTv.setGravity(Gravity.RIGHT);
        uTv.setTextAlignment(View.TEXT_ALIGNMENT_GRAVITY);
        uTv.setTextAppearance(R.style.textStyleE);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(0,12,0,12);
        uTv.setLayoutParams(params);

        return uTv;
    }
}
