package finthing.finthing.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import database_layer.FinthingDB;
import database_layer.TagsTB;
import database_layer.TransactionTB;
import finthing.finthing.R;
import finthing.finthing.adapter.TransactionAdapter;
import services.TransactionsIntentService;
import utils.Utils;

public class BankMainActivity extends AppCompatActivity {

    Activity mActivity;
    private BarChart monthlySpendsChart;
    private RecyclerView.Adapter mAdapter;
    private List<TransactionTB> transactionList;
    private HashMap<Integer, TagsTB> tagListMapping;
    private FinthingDB dbConn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_main);
        dbConn = FinthingDB.createDBInstance(getApplicationContext());
        mActivity = this;
        startTransactionsIntentService();
        prepareView();
        prepareMonthlyTransactionData();
        createMonthlySpendsChart();
        FinthingDB.destroyDBInstance();
    }

    private void prepareView(){
        monthlySpendsChart = (BarChart) findViewById(R.id.month_chart);
        prepareTransactionView();
    }

    private void prepareTransactionView(){
        transactionList = new ArrayList<>();
        tagListMapping = new HashMap<>();
        RecyclerView mTransactionRecyclerListView = (RecyclerView) findViewById(R.id.transaction_list);
        mTransactionRecyclerListView.setItemAnimator(new DefaultItemAnimator());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mTransactionRecyclerListView.setLayoutManager(mLayoutManager);
        mAdapter = new TransactionAdapter(transactionList, getApplicationContext(), tagListMapping);
        mTransactionRecyclerListView.setAdapter(mAdapter);
    }

    private void createMonthlySpendsChart(){
        new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        BarDataSet dataSet = new BarDataSet(prepareChartData(), "Label");
                        BarData barData = new BarData(dataSet);
                        barData.setBarWidth(0.6f);
                        monthlySpendsChart.setData(barData);
                        styleMonthlySpendsChart(dataSet); //styling
                        addChartLimitLine(1400, "₹34,000");
                        monthlySpendsChart.invalidate(); // refresh
                    }
                }
        ).start();
    }

    private List<BarEntry> prepareChartData(){
        List<BarEntry> graphData = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        long currentTime = calendar.getTimeInMillis();
        int currentDay =  calendar.get(Calendar.DAY_OF_MONTH);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE,0);
        calendar.set(Calendar.SECOND,0);
        long firstDayOfMonth = calendar.getTimeInMillis();
        List<TransactionTB> transData = dbConn.transactionTBDao().getTransactions(firstDayOfMonth, currentTime);
        float[] dayWiseSpends = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        for(TransactionTB trans: transData){
            if(trans.getAmount() < 0) {
                calendar.setTimeInMillis(trans.getTransTS());
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                dayWiseSpends[day] = dayWiseSpends[day] + (-1 * trans.getAmount().floatValue());
            }
        }
        float prev = 0;
        for (int i=1; i<dayWiseSpends.length; i++) {
            if(i > currentDay){
                graphData.add(new BarEntry(i, 0));
            }
            graphData.add(new BarEntry(i, dayWiseSpends[i]));
            prev = prev + dayWiseSpends[i];
        }
        return graphData;
    }

    private void styleMonthlySpendsChart(BarDataSet dataSet){
        float FONT_SIZE = 12f;
        monthlySpendsChart.getXAxis().setDrawGridLines(false);
        monthlySpendsChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        monthlySpendsChart.getXAxis().setDrawAxisLine(false);
        monthlySpendsChart.getXAxis().setAxisLineWidth(1.5f);
        monthlySpendsChart.getXAxis().setTextColor(Utils.getColor(getApplicationContext(), R.color.grey_light));
        monthlySpendsChart.getXAxis().setSpaceMin(2f);
        monthlySpendsChart.getXAxis().setTypeface(Utils.fontBold(getApplicationContext()));
        monthlySpendsChart.getXAxis().setTextSize(FONT_SIZE);
        monthlySpendsChart.getAxisLeft().setDrawGridLines(false);
        monthlySpendsChart.getAxisLeft().setTypeface(Utils.fontBold(getApplicationContext()));
        monthlySpendsChart.getAxisLeft().setTextSize(FONT_SIZE);
        monthlySpendsChart.getAxisLeft().setDrawAxisLine(false);
        monthlySpendsChart.getAxisLeft().setTextColor(Utils.getColor(getApplicationContext(), R.color.grey_light));
        monthlySpendsChart.getAxisRight().setDrawGridLines(false);
        monthlySpendsChart.getAxisRight().setEnabled(false);
        monthlySpendsChart.setScaleEnabled(false);
        monthlySpendsChart.getData().setHighlightEnabled(false);
        monthlySpendsChart.setExtraOffsets(0,0,0,10);
        monthlySpendsChart.getDescription().setEnabled(false);
        monthlySpendsChart.getLegend().setEnabled(false);
        dataSet.setColors(Utils.getColor(getApplicationContext(), R.color.grey_light));
        dataSet.setDrawValues(false);
    }

    private void addChartLimitLine(int yValue, String yValueText){
        LimitLine limitLine = new LimitLine(yValue, yValueText);
        limitLine.setLineWidth(2f);
        limitLine.setLineColor(Utils.getColor(getApplicationContext(), R.color.grey_light));
        limitLine.enableDashedLine(10,18,10);
        limitLine.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_TOP);
        limitLine.setTextColor(Utils.getColor(getApplicationContext(), R.color.black));
        limitLine.setTypeface(Utils.fontNormal(getApplicationContext()));
        monthlySpendsChart.getAxisLeft().addLimitLine(limitLine);
    }

    private void startTransactionsIntentService(){
        Intent transactionsService = new Intent(this, TransactionsIntentService.class);
        startService(transactionsService);
    }

    private void prepareMonthlyTransactionData(){
        new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        prepareTagList();
                        Calendar calendar = Calendar.getInstance();
                        long currentTime = calendar.getTimeInMillis();
                        calendar.set(Calendar.DAY_OF_MONTH, 1);
                        calendar.set(Calendar.HOUR_OF_DAY, 0);
                        calendar.set(Calendar.MINUTE,0);
                        calendar.set(Calendar.SECOND,0);
                        long firstDayOfMonth = calendar.getTimeInMillis();
                        transactionList.addAll(dbConn.transactionTBDao().getTransactions(firstDayOfMonth, currentTime));
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mAdapter.notifyDataSetChanged();
                            }
                        });
                    }
                }
        ).start();
    }

    private void prepareTagList(){
        List<TagsTB> tagList = dbConn.tagsTBDao().getTags();
        for(TagsTB tag: tagList){
            tagListMapping.put(tag.getId(), tag);
        }
    }



}
